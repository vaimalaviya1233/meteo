import 'package:meteo/presentation/pages/forecast_page.dart';
import 'package:meteo/presentation/pages/home/home_page.dart';
import 'package:meteo/presentation/pages/onboarding_page.dart';
import 'package:meteo/presentation/pages/splash_page.dart';

/// # Routes
/// use to manage routes navigation for this app
///
class Routes {
  static const String initial = "/";
  static const String onboarding = "/onboarding";
  static const String home = "/home";
  static const String forecast = "/forecast";

  // we need to define the routes binding here
  // we will use this in all routes application
  static var allRoutes = {
    initial: (context) => const SplashPage(),
    onboarding: (context) => const OnBoardingPage(),
    home: (context) => const HomePage(),
    forecast: (context) => const ForecastPage(),
  };
}
