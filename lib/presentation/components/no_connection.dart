import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';

/// # NoConnectionWidget
/// to show the connection status,
/// will show if the connection is failed, or disconnected
class NoConnectionWidget extends StatelessWidget {
  const NoConnectionWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        GestureDetector(
          onTap: () {
            // close this, when click
            if (Navigator.of(context).canPop()) {
              Navigator.of(context).pop();
            }
          },
          child: SizedBox(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
          ),
        ),

        // the main content
        Positioned(
          bottom: 32.0,
          left: 28.0,
          right: 28.0,
          child: Container(
            padding:
                const EdgeInsets.symmetric(horizontal: 20.0, vertical: 16.0),
            decoration: BoxDecoration(
              color: Theme.of(context).cardColor,
              borderRadius: BorderRadius.circular(28.0),
            ),
            child: Column(
              children: [
                const SizedBox(height: 32.0),
                const Icon(
                  IconlyBold.discovery,
                  size: 52.0,
                ),
                const SizedBox(height: 32.0),
                Text(
                  "No Connection",
                  style: Theme.of(context).textTheme.headline6,
                ),
                const SizedBox(height: 20.0),
                Text(
                  "Opps, no connection found, try to connect into another network",
                  style: Theme.of(context).textTheme.bodyText2,
                  textAlign: TextAlign.center,
                ),
                const SizedBox(height: 32.0),
                ElevatedButton(
                  style: ButtonStyle(
                    fixedSize: MaterialStateProperty.resolveWith(
                      (states) => const Size.fromHeight(48.0),
                    ),
                    padding: MaterialStateProperty.resolveWith(
                      (states) => const EdgeInsets.symmetric(horizontal: 20.0),
                    ),
                  ),
                  onPressed: () {},
                  child: const Text("Try Again"),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
