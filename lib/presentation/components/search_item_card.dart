import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:meteo/data/payloads/weather_payload.dart';
import 'package:meteo/logic/weather/weather_bloc.dart';

/// # SearchItemCard
///
/// item for search location
class SearchItemCard extends StatelessWidget {
  final WeatherLocationPayload item;

  const SearchItemCard({
    Key? key,
    required this.item,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        // we need to load the new weather, and then close the dialog
        context.read<WeatherBloc>().add(
              LoadWeatherByLocationPickedEvent(lat: item.lat, lon: item.lon),
            );
      },
      child: Container(
        margin: const EdgeInsets.only(bottom: 16.0),
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 12.0),
        decoration: BoxDecoration(
          border: Border.all(
            color: Theme.of(context).dividerColor,
          ),
          borderRadius: BorderRadius.circular(20.0),
        ),
        child: Row(
          children: [
            Container(
              height: 52.0,
              width: 52.0,
              decoration: BoxDecoration(
                color: Theme.of(context).primaryColor.withOpacity(0.1),
                borderRadius: BorderRadius.circular(16.0),
              ),
              child: Center(
                child: Icon(
                  IconlyBold.send,
                  color: Theme.of(context).primaryColor,
                ),
              ),
            ),
            const SizedBox(width: 16.0),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    item.name,
                    style: Theme.of(context).textTheme.subtitle2,
                  ),
                  const SizedBox(height: 8.0),
                  Text(
                    item.country,
                    style: Theme.of(context).textTheme.caption,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
