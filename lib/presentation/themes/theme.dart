import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:meteo/presentation/themes/colors.dart';

/// # Meteo App Theme
/// use to theming the application
/// using preference color, typography
class MeteoTheme {
  /// ## Light Theme
  /// use to show the custom theme for light
  static lightTheme() {
    return ThemeData(
      primaryColor: MeteoColors.lightPurpleBlue,
      colorScheme: const ColorScheme.light(
        primary: MeteoColors.lightPurpleBlue,
        background: MeteoColors.lightBackground,
        brightness: Brightness.light,
      ),
      brightness: Brightness.light,
      fontFamily: GoogleFonts.poppins().fontFamily,
      backgroundColor: MeteoColors.lightBackground,
      scaffoldBackgroundColor: MeteoColors.lightBackground,
      cardColor: MeteoColors.lightCard,
      hintColor: MeteoColors.lightLighterGrey,
      errorColor: Colors.red,
      focusColor: MeteoColors.lightPurpleBlue,
      hoverColor: MeteoColors.lightLighterGrey.withOpacity(0.3),
      splashColor: Colors.white,
      dividerColor: MeteoColors.lightLighterGrey.withOpacity(0.1),
      disabledColor: MeteoColors.lightLighterGrey.withOpacity(0.1),
      indicatorColor: MeteoColors.lightPurpleBlue,
      dialogBackgroundColor: Colors.white,
      unselectedWidgetColor: MeteoColors.lightLighterGrey.withOpacity(0.2),
      toggleableActiveColor: MeteoColors.lightPurpleBlue,
      primaryColorBrightness: Brightness.light,

      // appbar theme
      appBarTheme: AppBarTheme(
        color: Colors.transparent,
        elevation: 0.0,
        centerTitle: true,
        titleTextStyle: GoogleFonts.poppins().copyWith(
          fontWeight: FontWeight.w700,
          fontSize: 20.0,
          color: MeteoColors.lightPurpleDark,
        ),
        foregroundColor: MeteoColors.lightPurpleDark,
      ),

      // text theme
      textTheme: TextTheme(
        headline1: GoogleFonts.poppins().copyWith(
          fontSize: 32.0,
          fontWeight: FontWeight.bold,
          color: MeteoColors.lightPurpleDark,
        ),
        headline2: GoogleFonts.poppins().copyWith(
          fontSize: 28.0,
          fontWeight: FontWeight.bold,
          color: MeteoColors.lightPurpleDark,
        ),
        headline3: GoogleFonts.poppins().copyWith(
          fontSize: 26.0,
          fontWeight: FontWeight.bold,
          color: MeteoColors.lightPurpleDark,
        ),
        headline4: GoogleFonts.poppins().copyWith(
          fontSize: 24.0,
          fontWeight: FontWeight.bold,
          color: MeteoColors.lightPurpleDark,
        ),
        headline5: GoogleFonts.poppins().copyWith(
          fontSize: 20.0,
          fontWeight: FontWeight.bold,
          color: MeteoColors.lightPurpleDark,
        ),
        headline6: GoogleFonts.poppins().copyWith(
          fontSize: 18.0,
          fontWeight: FontWeight.bold,
          color: MeteoColors.lightPurpleDark,
        ),
        subtitle1: GoogleFonts.poppins().copyWith(
          fontSize: 16.0,
          fontWeight: FontWeight.w600,
          color: MeteoColors.lightPurpleDark,
        ),
        subtitle2: GoogleFonts.poppins().copyWith(
          fontSize: 15.0,
          fontWeight: FontWeight.w600,
          color: MeteoColors.lightPurpleDark,
        ),
        bodyText1: GoogleFonts.poppins().copyWith(
          fontSize: 15.0,
          height: 1.6,
          fontWeight: FontWeight.normal,
          color: MeteoColors.lightGrey,
        ),
        bodyText2: GoogleFonts.poppins().copyWith(
          fontSize: 14.0,
          height: 1.6,
          fontWeight: FontWeight.normal,
          color: MeteoColors.lightGrey,
        ),
        button: GoogleFonts.poppins().copyWith(
          fontSize: 14.0,
          fontWeight: FontWeight.w600,
          color: MeteoColors.lightPurpleDark,
        ),
        caption: GoogleFonts.poppins().copyWith(
          fontSize: 12.0,
          fontWeight: FontWeight.normal,
          color: MeteoColors.lightGrey,
        ),
      ),

      // icon theme,
      // customize the look of much icon
      iconTheme: const IconThemeData(
        color: MeteoColors.lightPurpleDark,
        size: 24.0,
      ),
      dividerTheme: DividerThemeData(
        color: MeteoColors.lightLighterGrey.withOpacity(0.3),
      ),

      // tabbar theme
      // change the general look of tabbar
      tabBarTheme: TabBarTheme(
        unselectedLabelColor: Colors.white.withOpacity(0.5),
        labelColor: MeteoColors.lightPurpleBlue,
        indicator: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(16.0),
        ),
        labelStyle: GoogleFonts.poppins().copyWith(
          fontSize: 14.0,
          fontWeight: FontWeight.w600,
        ),
        unselectedLabelStyle: GoogleFonts.poppins().copyWith(
          fontSize: 14.0,
          fontWeight: FontWeight.w600,
        ),
      ),

      // button theme,
      // including elevated, outlined, etc.
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.resolveWith(
              (states) => MeteoColors.lightPurpleBlue),
          elevation: MaterialStateProperty.resolveWith((states) => 0.0),
          shape: MaterialStateProperty.resolveWith(
            (states) => RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16.0)),
          ),
          fixedSize: MaterialStateProperty.resolveWith(
              (states) => const Size.fromHeight(56.0)),
        ),
      ),
      textButtonTheme: TextButtonThemeData(
        style: ButtonStyle(
          foregroundColor: MaterialStateProperty.resolveWith(
              (states) => MeteoColors.lightPurpleDark),
        ),
      ),
      bottomSheetTheme: const BottomSheetThemeData(
        backgroundColor: Colors.white,
        elevation: 0.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(28.0),
            topRight: Radius.circular(28.0),
          ),
        ),
        modalBackgroundColor: Colors.white,
      ),
    );
  }

  /// ## Dark Theme
  /// use to show the custom theme for darkmode
  static darkTheme() {
    return ThemeData(
      primaryColor: MeteoColors.darkPurpleBlue,
      colorScheme: const ColorScheme.light(
        primary: MeteoColors.darkPurpleBlue,
        background: MeteoColors.darkBackground,
        brightness: Brightness.dark,
      ),
      brightness: Brightness.dark,
      fontFamily: GoogleFonts.poppins().fontFamily,
      backgroundColor: MeteoColors.darkBackground,
      scaffoldBackgroundColor: MeteoColors.darkBackground,
      cardColor: MeteoColors.darkCard,
      hintColor: MeteoColors.darkDarkerGrey,
      errorColor: Colors.red,
      focusColor: MeteoColors.darkPurpleBlue,
      hoverColor: MeteoColors.darkDarkerGrey.withOpacity(0.3),
      splashColor: Colors.black38,
      dividerColor: MeteoColors.darkDarkerGrey.withOpacity(0.1),
      disabledColor: MeteoColors.darkDarkerGrey.withOpacity(0.1),
      indicatorColor: MeteoColors.darkPurpleBlue,
      dialogBackgroundColor: MeteoColors.darkCard,
      unselectedWidgetColor: MeteoColors.darkDarkerGrey.withOpacity(0.2),
      toggleableActiveColor: MeteoColors.darkPurpleBlue,
      primaryColorBrightness: Brightness.dark,

      // appbar theme
      appBarTheme: AppBarTheme(
          color: Colors.transparent,
          elevation: 0.0,
          centerTitle: true,
          titleTextStyle: GoogleFonts.poppins().copyWith(
            fontWeight: FontWeight.w700,
            fontSize: 20.0,
            color: MeteoColors.darkPurpleLight,
          ),
          foregroundColor: MeteoColors.darkPurpleLight),

      // text theme
      textTheme: TextTheme(
        headline1: GoogleFonts.poppins().copyWith(
          fontSize: 32.0,
          fontWeight: FontWeight.bold,
          color: MeteoColors.darkPurpleLight,
        ),
        headline2: GoogleFonts.poppins().copyWith(
          fontSize: 28.0,
          fontWeight: FontWeight.bold,
          color: MeteoColors.darkPurpleLight,
        ),
        headline3: GoogleFonts.poppins().copyWith(
          fontSize: 26.0,
          fontWeight: FontWeight.bold,
          color: MeteoColors.darkPurpleLight,
        ),
        headline4: GoogleFonts.poppins().copyWith(
          fontSize: 24.0,
          fontWeight: FontWeight.bold,
          color: MeteoColors.darkPurpleLight,
        ),
        headline5: GoogleFonts.poppins().copyWith(
          fontSize: 20.0,
          fontWeight: FontWeight.bold,
          color: MeteoColors.darkPurpleLight,
        ),
        headline6: GoogleFonts.poppins().copyWith(
          fontSize: 18.0,
          fontWeight: FontWeight.bold,
          color: MeteoColors.darkPurpleLight,
        ),
        subtitle1: GoogleFonts.poppins().copyWith(
          fontSize: 16.0,
          fontWeight: FontWeight.w600,
          color: MeteoColors.darkPurpleLight,
        ),
        subtitle2: GoogleFonts.poppins().copyWith(
          fontSize: 15.0,
          fontWeight: FontWeight.w600,
          color: MeteoColors.darkPurpleLight,
        ),
        bodyText1: GoogleFonts.poppins().copyWith(
          fontSize: 15.0,
          height: 1.6,
          fontWeight: FontWeight.normal,
          color: MeteoColors.darkGrey,
        ),
        bodyText2: GoogleFonts.poppins().copyWith(
          fontSize: 14.0,
          height: 1.6,
          fontWeight: FontWeight.normal,
          color: MeteoColors.darkGrey,
        ),
        button: GoogleFonts.poppins().copyWith(
          fontSize: 14.0,
          fontWeight: FontWeight.w600,
          color: MeteoColors.darkPurpleLight,
        ),
        caption: GoogleFonts.poppins().copyWith(
          fontSize: 12.0,
          fontWeight: FontWeight.normal,
          color: MeteoColors.darkGrey,
        ),
      ),

      // icon theme,
      // customize the look of much icon
      iconTheme: const IconThemeData(
        color: MeteoColors.darkPurpleLight,
        size: 24.0,
      ),
      dividerTheme: DividerThemeData(
        color: MeteoColors.darkDarkerGrey.withOpacity(0.3),
      ),

      // tabbar theme
      // change the general look of tabbar
      tabBarTheme: TabBarTheme(
        unselectedLabelColor: Colors.white.withOpacity(0.5),
        labelColor: MeteoColors.lightPurpleBlue,
        indicator: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(16.0),
        ),
        labelStyle: GoogleFonts.poppins().copyWith(
          fontSize: 14.0,
          fontWeight: FontWeight.w600,
        ),
        unselectedLabelStyle: GoogleFonts.poppins().copyWith(
          fontSize: 14.0,
          fontWeight: FontWeight.w600,
        ),
      ),

      // button theme,
      // including elevated, outlined, etc.
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.resolveWith(
              (states) => MeteoColors.darkPurpleBlue),
          elevation: MaterialStateProperty.resolveWith((states) => 0.0),
          shape: MaterialStateProperty.resolveWith(
            (states) => RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(16.0)),
          ),
          fixedSize: MaterialStateProperty.resolveWith(
              (states) => const Size.fromHeight(56.0)),
        ),
      ),
      textButtonTheme: TextButtonThemeData(
        style: ButtonStyle(
          foregroundColor: MaterialStateProperty.resolveWith(
              (states) => MeteoColors.darkPurpleLight),
        ),
      ),
      bottomSheetTheme: const BottomSheetThemeData(
        backgroundColor: MeteoColors.darkCard,
        elevation: 0.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(28.0),
            topRight: Radius.circular(28.0),
          ),
        ),
        modalBackgroundColor: MeteoColors.darkCard,
      ),
    );
  }
}
