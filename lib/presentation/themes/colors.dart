import 'package:flutter/material.dart';

/// # MeteoColors
/// specify and define the custom color
class MeteoColors {
  // specify to light colors, theme
  static const Color lightBackground = Color(0xFFFAFAFA);
  static const Color lightPurpleBlue = Color(0xFF6D39FF);
  static const Color lightPurpleDark = Color(0xFF46386D);
  static const Color lightGrey = Color(0xFF605776);
  static const Color lightLighterGrey = Color(0xFF969696);
  static const Color lightCard = Color(0xFFFFFFFF);

  // specify to dark colors, theme
  static const Color darkBackground = Color(0xFF1D0F39);
  static const Color darkPurpleBlue = Color(0xFF5C30DC);
  static const Color darkPurpleLight = Color(0xFFE5E0F4);
  static const Color darkGrey = Color(0xFFBDB8CB);
  static const Color darkDarkerGrey = Color(0xFF7D7888);
  static const Color darkCard = Color(0xFF2B1B4B);

  // add here, if you had another one theme colors
}
