import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meteo/logic/weather/weather_bloc.dart';
import 'package:meteo/presentation/components/day_forecast_item_card.dart';
import 'package:meteo/presentation/themes/behavior.dart';
import 'package:meteo/presentation/themes/colors.dart';
import 'package:meteo/presentation/utils/weather_icon_decoder.dart';

/// # ForecastPage
/// detail forecast for the weather
/// the user will be able to see the range of day for forecast
class ForecastPage extends StatefulWidget {
  const ForecastPage({Key? key}) : super(key: key);

  @override
  _ForecastPageState createState() => _ForecastPageState();
}

class _ForecastPageState extends State<ForecastPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("7 Days Forecast"),
      ),
      body: ScrollConfiguration(
        behavior: CleanScrollBehavior(),
        child: SingleChildScrollView(
          padding: const EdgeInsets.symmetric(horizontal: 28.0, vertical: 20.0),
          child: BlocBuilder<WeatherBloc, WeatherState>(
            builder: (context, state) {
              if (state is SuccessWeatherState) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // we have some sumary icon, and label
                    const SizedBox(height: 48.0),
                    Center(
                      child: Image.asset(
                        weatherIconDecoder(
                            state.forecast.current.condition.code,
                            state.forecast.current.is_day),
                        width: 170.0,
                        height: 170.0,
                        fit: BoxFit.contain,
                      ),
                    ),

                    // the condition
                    const SizedBox(height: 20.0),
                    Center(
                      child: ShaderMask(
                        shaderCallback: (bounds) {
                          return LinearGradient(
                            colors: Theme.of(context).primaryColorBrightness ==
                                    Brightness.light
                                ? [
                                    MeteoColors.lightPurpleDark,
                                    MeteoColors.lightPurpleDark.withOpacity(0),
                                  ]
                                : [
                                    MeteoColors.darkPurpleLight,
                                    MeteoColors.darkPurpleLight.withOpacity(0),
                                  ],
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                          ).createShader(Offset.zero & bounds.size);
                        },
                        child: Text(
                          state.forecast.current.condition.text,
                          style:
                              Theme.of(context).textTheme.headline1!.copyWith(
                                    color: Colors.white,
                                  ),
                        ),
                      ),
                    ),

                    // summary of the weather that selected, or recent
                    Container(
                      margin: const EdgeInsets.fromLTRB(12.0, 24.0, 12.0, 0.0),
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20.0, vertical: 16.0),
                      decoration: BoxDecoration(
                        color: Theme.of(context).cardColor,
                        borderRadius: BorderRadius.circular(20.0),
                        boxShadow: [
                          BoxShadow(
                            blurRadius: 30.0,
                            color: Colors.grey.withOpacity(0.1),
                          ),
                        ],
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          // wind
                          Column(
                            children: [
                              Text(
                                "Temp",
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText2!
                                    .copyWith(
                                      color: Theme.of(context).hintColor,
                                    ),
                              ),
                              const SizedBox(height: 4.0),
                              RichText(
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text:
                                          "${state.forecast.current.temp_c.round()}°",
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline4!
                                          .copyWith(),
                                    ),
                                    TextSpan(
                                      text: " C",
                                      style:
                                          Theme.of(context).textTheme.caption,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),

                          const SizedBox(
                            height: 30.0,
                            child: VerticalDivider(
                              width: 1.0,
                            ),
                          ),

                          // wind
                          Column(
                            children: [
                              Text(
                                "Wind",
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText2!
                                    .copyWith(
                                        color: Theme.of(context).hintColor),
                              ),
                              const SizedBox(height: 4.0),
                              RichText(
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text:
                                          "${state.forecast.current.wind_mph.round()}",
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline4!
                                          .copyWith(),
                                    ),
                                    TextSpan(
                                      text: " mph",
                                      style:
                                          Theme.of(context).textTheme.caption,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),

                          // spacer
                          const SizedBox(
                            height: 30.0,
                            child: VerticalDivider(
                              width: 1.0,
                            ),
                          ),

                          // wind
                          Column(
                            children: [
                              Text(
                                "Humidity",
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText2!
                                    .copyWith(
                                      color: Theme.of(context).hintColor,
                                    ),
                              ),
                              const SizedBox(height: 4.0),
                              RichText(
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text:
                                          "${state.forecast.current.humidity.round()}",
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline4!
                                          .copyWith(),
                                    ),
                                    TextSpan(
                                      text: " %",
                                      style:
                                          Theme.of(context).textTheme.caption,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),

                    // forecast
                    // for a week
                    const SizedBox(height: 40.0),
                    Text(
                      "Next 7 Days",
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                    const SizedBox(height: 20.0),
                    ListView.builder(
                      itemCount: state.forecast.forecast.forecastday.length,
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemBuilder: (context, index) {
                        var item = state.forecast.forecast.forecastday[index];
                        return DayForecastItemCard(item: item);
                      },
                    ),
                  ],
                );
              } else {
                return const SizedBox.shrink();
              }
            },
          ),
        ),
      ),
    );
  }
}
