import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:iconly/iconly.dart';
import 'package:intl/intl.dart';
import 'package:meteo/logic/weather/weather_bloc.dart';
import 'package:meteo/presentation/pages/home/widgets/home_search_widget.dart';
import 'package:meteo/presentation/themes/behavior.dart';
import 'package:meteo/presentation/themes/colors.dart';
import 'package:meteo/presentation/utils/weather_icon_decoder.dart';
import 'package:shimmer/shimmer.dart';

/// # MainHomeWidget
/// the main content for home,
/// see the all of main data, summary come here
class MainHomeWidget extends StatefulWidget {
  const MainHomeWidget({Key? key}) : super(key: key);

  @override
  _MainHomeWidgetState createState() => _MainHomeWidgetState();
}

class _MainHomeWidgetState extends State<MainHomeWidget> {
  // show the search view
  // allow user to see the search location, and listed
  // user will allow to pick their references users
  void _showSearchView() {
    showModalBottomSheet(
      context: context,
      constraints:
          BoxConstraints(maxHeight: MediaQuery.of(context).size.height - 120.0),
      isScrollControlled: true,
      barrierColor: Colors.transparent,
      elevation: 0.0,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(28.0),
          topRight: Radius.circular(28.0),
        ),
      ),
      enableDrag: true,
      builder: (context) {
        return const HomeSearchWidget();
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.fromLTRB(28.0, 64, 28.0, 0.0),
      child: BlocConsumer<WeatherBloc, WeatherState>(
        listener: (context, state) {
          // well, here we will close the search bootomsheet
          // when the weather load
          if (state is SuccessWeatherState) {
            // close the dialog
            if (Navigator.of(context).canPop()) Navigator.of(context).pop();
          }
        },
        builder: (context, state) {
          // the state was successful
          // show the actual data into user
          if (state is SuccessWeatherState) {
            return ScrollConfiguration(
              behavior: CleanScrollBehavior(),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // header
                    Row(
                      children: [
                        // we have some greating, and headline
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                DateFormat("EEEE, dd MMMM yyyy")
                                    .format(DateTime.now()),
                                style: Theme.of(context)
                                    .textTheme
                                    .subtitle1!
                                    .copyWith(
                                      color: Theme.of(context).hintColor,
                                    ),
                              ),
                              Text(
                                state.forecast.location.name,
                                style: Theme.of(context).textTheme.headline4,
                              ),
                            ],
                          ),
                        ),

                        // we have and search location button,
                        // to update the location
                        Container(
                          height: 48.0,
                          width: 48.0,
                          decoration: BoxDecoration(
                            color: Theme.of(context)
                                .disabledColor
                                .withOpacity(0.1),
                            borderRadius: BorderRadius.circular(16.0),
                          ),
                          child: IconButton(
                            onPressed: () {
                              // show the search view
                              _showSearchView();
                            },
                            icon: const Icon(
                              IconlyLight.search,
                              size: 24.0,
                            ),
                          ),
                        ),
                      ],
                    ),

                    // we have some sumary icon, and label
                    const SizedBox(height: 24.0),
                    Center(
                      child: // image of weather
                          Image.asset(
                        weatherIconDecoder(
                            state.forecast.current.condition.code,
                            state.forecast.current.is_day),
                        width: 200.0,
                        height: 200.0,
                        fit: BoxFit.contain,
                      ),
                    ),

                    // the condition
                    Center(
                      child: ShaderMask(
                        shaderCallback: (bounds) {
                          return LinearGradient(
                            colors: Theme.of(context).primaryColorBrightness ==
                                    Brightness.light
                                ? [
                                    MeteoColors.lightPurpleDark,
                                    MeteoColors.lightPurpleDark.withOpacity(0),
                                  ]
                                : [
                                    MeteoColors.darkPurpleLight,
                                    MeteoColors.darkPurpleLight.withOpacity(0),
                                  ],
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                          ).createShader(Offset.zero & bounds.size);
                        },
                        child: Text(
                          state.forecast.current.condition.text,
                          style:
                              Theme.of(context).textTheme.headline1!.copyWith(
                                    color: Colors.white,
                                  ),
                        ),
                      ),
                    ),

                    // summary of the weather that selected, or recent
                    Container(
                      margin: const EdgeInsets.fromLTRB(12.0, 24.0, 12.0, 0.0),
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20.0, vertical: 16.0),
                      decoration: BoxDecoration(
                        color: Theme.of(context).cardColor,
                        borderRadius: BorderRadius.circular(20.0),
                        boxShadow: [
                          BoxShadow(
                            blurRadius: 30.0,
                            color:
                                Theme.of(context).shadowColor.withOpacity(0.02),
                          ),
                        ],
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          // wind
                          Column(
                            children: [
                              Text(
                                "Temp",
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText2!
                                    .copyWith(
                                      color: Theme.of(context).hintColor,
                                    ),
                              ),
                              const SizedBox(height: 4.0),
                              RichText(
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text:
                                          "${state.forecast.current.temp_c.round()}°",
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline4!
                                          .copyWith(),
                                    ),
                                    TextSpan(
                                      text: " C",
                                      style:
                                          Theme.of(context).textTheme.caption,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),

                          const SizedBox(
                            height: 30.0,
                            child: VerticalDivider(
                              width: 1.0,
                            ),
                          ),

                          // wind
                          Column(
                            children: [
                              Text(
                                "Wind",
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText2!
                                    .copyWith(
                                      color: Theme.of(context).hintColor,
                                    ),
                              ),
                              const SizedBox(height: 4.0),
                              RichText(
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text:
                                          "${state.forecast.current.wind_mph.round()}",
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline4!
                                          .copyWith(),
                                    ),
                                    TextSpan(
                                      text: " mph",
                                      style:
                                          Theme.of(context).textTheme.caption,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),

                          // spacer
                          const SizedBox(
                            height: 30.0,
                            child: VerticalDivider(
                              width: 1.0,
                            ),
                          ),

                          // wind
                          Column(
                            children: [
                              Text(
                                "Humidity",
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText2!
                                    .copyWith(
                                      color: Theme.of(context).hintColor,
                                    ),
                              ),
                              const SizedBox(height: 4.0),
                              RichText(
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text:
                                          "${state.forecast.current.humidity.round()}",
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline4!
                                          .copyWith(),
                                    ),
                                    TextSpan(
                                      text: " %",
                                      style:
                                          Theme.of(context).textTheme.caption,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 300.0)
                  ],
                ),
              ),
            );
          } else {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(height: 20.0),
                Shimmer.fromColors(
                  child: Container(
                    height: 24.0,
                    width: 200.0,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                  ),
                  baseColor: Theme.of(context).cardColor.withOpacity(0.4),
                  highlightColor: Theme.of(context).cardColor,
                ),
                const SizedBox(height: 12.0),
                Shimmer.fromColors(
                  child: Container(
                    height: 40.0,
                    width: 140.0,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                  ),
                  baseColor: Theme.of(context).cardColor.withOpacity(0.4),
                  highlightColor: Theme.of(context).cardColor,
                ),
                const SizedBox(height: 40.0),
                Center(
                  child: Shimmer.fromColors(
                    child: Container(
                      height: 140.0,
                      width: 1040.0,
                      decoration: const BoxDecoration(
                          color: Colors.white, shape: BoxShape.circle),
                    ),
                    baseColor: Theme.of(context).cardColor.withOpacity(0.4),
                    highlightColor: Theme.of(context).cardColor,
                  ),
                ),
                const SizedBox(height: 32.0),
                Center(
                  child: Shimmer.fromColors(
                    child: Container(
                      height: 40.0,
                      width: 140.0,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(12.0),
                      ),
                    ),
                    baseColor: Theme.of(context).cardColor.withOpacity(0.4),
                    highlightColor: Theme.of(context).cardColor,
                  ),
                ),
                const SizedBox(height: 24.0),
                const SizedBox(height: 12.0),
                Shimmer.fromColors(
                  child: Container(
                    height: 120.0,
                    width: MediaQuery.of(context).size.width - 56.0,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                  ),
                  baseColor: Theme.of(context).cardColor.withOpacity(0.4),
                  highlightColor: Theme.of(context).cardColor,
                ),
              ],
            );
          }
        },
      ),
    );
  }
}
