import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meteo/logic/weather/weather_bloc.dart';
import 'package:meteo/presentation/themes/colors.dart';
import 'package:meteo/presentation/utils/weather_icon_decoder.dart';
import 'package:shimmer/shimmer.dart';

/// # HomeDetailInfo
/// the detail widget
/// to show the summary data like, temp, air, etc.
class HomeDetailInfoWidget extends StatelessWidget {
  const HomeDetailInfoWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<WeatherBloc, WeatherState>(
      builder: (context, state) {
        // the state was successfuly
        // we need to show the real data into user
        if (state is SuccessWeatherState) {
          return SingleChildScrollView(
            child: Column(
              children: [
                const SizedBox(height: 48.0),
                Center(
                  child: Image.asset(
                    weatherIconDecoder(state.forecast.current.condition.code,
                        state.forecast.current.is_day),
                    width: 170.0,
                    height: 170.0,
                  ),
                ),
                const SizedBox(height: 20.0),
                ShaderMask(
                  shaderCallback: (bouds) {
                    return LinearGradient(
                      colors: [
                        MeteoColors.darkPurpleLight,
                        MeteoColors.darkPurpleLight.withOpacity(0.1),
                      ],
                    ).createShader(Offset.zero & bouds.size);
                  },
                  child: Text(
                    state.forecast.current.condition.text,
                    style: Theme.of(context).textTheme.headline1!.copyWith(
                          color: Colors.white,
                        ),
                  ),
                ),

                // // conditions
                // const SizedBox(height: 28.0),
                // Center(
                //   child: Container(
                //     padding: const EdgeInsets.symmetric(
                //         horizontal: 12.0, vertical: 8.0),
                //     decoration: BoxDecoration(
                //       color: Colors.white.withOpacity(0.05),
                //       borderRadius: BorderRadius.circular(12.0),
                //     ),
                //     child: Text(
                //       "Good",
                //       style: Theme.of(context).textTheme.subtitle1!.copyWith(
                //             color: Colors.white.withOpacity(0.8),
                //           ),
                //     ),
                //   ),
                // ),

                // summary
                // contain many item and info like a temp, wind, cloud, etc.
                Container(
                  margin: const EdgeInsets.fromLTRB(28.0, 24.0, 28.0, 40.0),
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 16.0),
                  decoration: BoxDecoration(
                    color: Colors.white.withOpacity(0.06),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          // temp
                          Column(
                            children: [
                              Text(
                                "Temp",
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText2!
                                    .copyWith(
                                      color: Colors.white54,
                                    ),
                              ),
                              const SizedBox(height: 4.0),
                              RichText(
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text:
                                          "${state.forecast.current.temp_c.round()}°",
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline4!
                                          .copyWith(
                                            color:
                                                Colors.white.withOpacity(0.8),
                                          ),
                                    ),
                                    TextSpan(
                                      text: " C",
                                      style: Theme.of(context)
                                          .textTheme
                                          .caption!
                                          .copyWith(
                                            color:
                                                Colors.white.withOpacity(0.4),
                                          ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),

                          const SizedBox(
                            height: 30.0,
                            child: VerticalDivider(
                              width: 1.0,
                              color: Colors.white24,
                            ),
                          ),

                          // wind
                          Column(
                            children: [
                              Text(
                                "Wind",
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText2!
                                    .copyWith(
                                      color: Colors.white54,
                                    ),
                              ),
                              const SizedBox(height: 4.0),
                              RichText(
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text:
                                          "${state.forecast.current.wind_mph.round()}",
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline4!
                                          .copyWith(
                                            color:
                                                Colors.white.withOpacity(0.8),
                                          ),
                                    ),
                                    TextSpan(
                                      text: " mph",
                                      style: Theme.of(context)
                                          .textTheme
                                          .caption!
                                          .copyWith(
                                            color:
                                                Colors.white.withOpacity(0.4),
                                          ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),

                          const SizedBox(
                            height: 30.0,
                            child: VerticalDivider(
                              width: 1.0,
                              color: Colors.white24,
                            ),
                          ),

                          // Humidity
                          Column(
                            children: [
                              Text(
                                "Humidity",
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText2!
                                    .copyWith(
                                      color: Colors.white54,
                                    ),
                              ),
                              const SizedBox(height: 4.0),
                              RichText(
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text:
                                          "${state.forecast.current.humidity.round()}",
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline4!
                                          .copyWith(
                                            color:
                                                Colors.white.withOpacity(0.8),
                                          ),
                                    ),
                                    TextSpan(
                                      text: " %",
                                      style: Theme.of(context)
                                          .textTheme
                                          .caption!
                                          .copyWith(
                                            color:
                                                Colors.white.withOpacity(0.4),
                                          ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      const SizedBox(height: 24.0),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          // precip
                          Column(
                            children: [
                              Text(
                                "Precip",
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText2!
                                    .copyWith(
                                      color: Colors.white54,
                                    ),
                              ),
                              const SizedBox(height: 4.0),
                              RichText(
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text:
                                          "${state.forecast.current.precip_mm.round()}",
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline4!
                                          .copyWith(
                                            color:
                                                Colors.white.withOpacity(0.8),
                                          ),
                                    ),
                                    TextSpan(
                                      text: " mm",
                                      style: Theme.of(context)
                                          .textTheme
                                          .caption!
                                          .copyWith(
                                            color:
                                                Colors.white.withOpacity(0.4),
                                          ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),

                          const SizedBox(
                            height: 30.0,
                            child: VerticalDivider(
                              width: 1.0,
                              color: Colors.white24,
                            ),
                          ),

                          // press
                          Column(
                            children: [
                              Text(
                                "Press",
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText2!
                                    .copyWith(
                                      color: Colors.white54,
                                    ),
                              ),
                              const SizedBox(height: 4.0),
                              RichText(
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text:
                                          "${state.forecast.current.pressure_mb.round()}",
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline4!
                                          .copyWith(
                                            color:
                                                Colors.white.withOpacity(0.8),
                                          ),
                                    ),
                                    TextSpan(
                                      text: " mb",
                                      style: Theme.of(context)
                                          .textTheme
                                          .caption!
                                          .copyWith(
                                            color:
                                                Colors.white.withOpacity(0.4),
                                          ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),

                          const SizedBox(
                            height: 30.0,
                            child: VerticalDivider(
                              width: 1.0,
                              color: Colors.white24,
                            ),
                          ),

                          // Cloud
                          Column(
                            children: [
                              Text(
                                "Cloud",
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText2!
                                    .copyWith(
                                      color: Colors.white54,
                                    ),
                              ),
                              const SizedBox(height: 4.0),
                              RichText(
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text:
                                          "${state.forecast.current.cloud.round()}",
                                      style: Theme.of(context)
                                          .textTheme
                                          .headline4!
                                          .copyWith(
                                            color:
                                                Colors.white.withOpacity(0.8),
                                          ),
                                    ),
                                    TextSpan(
                                      text: " %",
                                      style: Theme.of(context)
                                          .textTheme
                                          .caption!
                                          .copyWith(
                                            color:
                                                Colors.white.withOpacity(0.4),
                                          ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
          );
        } else {
          return SingleChildScrollView(
            padding: const EdgeInsets.symmetric(horizontal: 28.0),
            child: Column(
              children: [
                const SizedBox(height: 20.0),
                const SizedBox(height: 40.0),
                Center(
                  child: Shimmer.fromColors(
                    child: Container(
                      height: 140.0,
                      width: 1040.0,
                      decoration: const BoxDecoration(
                          color: Colors.white, shape: BoxShape.circle),
                    ),
                    baseColor: Colors.white.withOpacity(0.04),
                    highlightColor: Colors.white.withOpacity(0.05),
                  ),
                ),
                const SizedBox(height: 32.0),
                Center(
                  child: Shimmer.fromColors(
                    child: Container(
                      height: 40.0,
                      width: 140.0,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(12.0),
                      ),
                    ),
                    baseColor: Colors.white.withOpacity(0.04),
                    highlightColor: Colors.white.withOpacity(0.05),
                  ),
                ),
                const SizedBox(height: 24.0),
                const SizedBox(height: 12.0),
                Shimmer.fromColors(
                  child: Container(
                    height: 120.0,
                    width: MediaQuery.of(context).size.width - 56.0,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(12.0),
                    ),
                  ),
                  baseColor: Colors.white.withOpacity(0.04),
                  highlightColor: Colors.white.withOpacity(0.05),
                ),
              ],
            ),
          );
        }
      },
    );
  }
}
