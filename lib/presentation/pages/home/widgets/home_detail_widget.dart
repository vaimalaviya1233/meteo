import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:iconly/iconly.dart';
import 'package:meteo/logic/weather/weather_bloc.dart';
import 'package:meteo/presentation/components/hour_forecast_item_card.dart';
import 'package:meteo/presentation/pages/home/widgets/home_detail_alert_widget.dart';
import 'package:meteo/presentation/pages/home/widgets/home_detail_astronomy_widget.dart';
import 'package:meteo/presentation/pages/home/widgets/home_detail_info_widget.dart';
import 'package:meteo/presentation/pages/home/widgets/home_detail_sport_widget.dart';
import 'package:meteo/presentation/routes/routes.dart';
import 'package:meteo/presentation/themes/behavior.dart';
import 'package:shimmer/shimmer.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

/// # HomeDetailWidget
/// show the detail info for weather
class HomeDetailWidget extends StatefulWidget {
  const HomeDetailWidget({Key? key}) : super(key: key);

  @override
  _HomeDetailWidgetState createState() => _HomeDetailWidgetState();
}

class _HomeDetailWidgetState extends State<HomeDetailWidget>
    with SingleTickerProviderStateMixin {
  // widget variables
  late TabController _tabController;

  @override
  void initState() {
    super.initState();

    _tabController = TabController(length: 4, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return SlidingUpPanel(
      borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(28.0), topRight: Radius.circular(28.0)),
      boxShadow: [
        BoxShadow(
          blurRadius: 60.0,
          offset: const Offset(0.0, -20),
          color: Theme.of(context).primaryColor.withOpacity(0.3),
        ),
      ],
      color: Theme.of(context).primaryColor,
      minHeight: 280.0,
      maxHeight: MediaQuery.of(context).size.height - 120.0,
      header: Container(
        width: MediaQuery.of(context).size.width,
        margin: const EdgeInsets.only(top: 12.0),
        child: Center(
          child: Container(
            width: 44.0,
            height: 6.0,
            decoration: BoxDecoration(
              color: Colors.white.withOpacity(0.2),
              borderRadius: BorderRadius.circular(12.0),
            ),
          ),
        ),
      ),
      collapsed: Container(
        margin: const EdgeInsets.only(top: 30.0),
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
        ),
        child: Column(
          children: [
            // header
            const SizedBox(height: 10.0),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 28.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Today",
                    style: Theme.of(context).textTheme.subtitle1!.copyWith(
                          color: Colors.white.withOpacity(0.8),
                        ),
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.pushNamed(context, Routes.forecast);
                    },
                    style: ButtonStyle(
                      foregroundColor: MaterialStateProperty.resolveWith(
                        (states) => Colors.white.withOpacity(0.8),
                      ),
                    ),
                    child: Row(
                      children: [
                        const Text("Next 7 Days"),
                        Icon(
                          IconlyLight.arrow_right_2,
                          color: Colors.white.withOpacity(0.8),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),

            // forecast list
            // allow, user to see the forecast in every hour in day
            // about the summmary and conditions
            const SizedBox(height: 32.0),
            BlocBuilder<WeatherBloc, WeatherState>(
              builder: (context, state) {
                // show the actual data to user
                // the process is successful
                if (state is SuccessWeatherState) {
                  return SizedBox(
                    height: 150.0,
                    child: ScrollConfiguration(
                      behavior: CleanScrollBehavior(),
                      child: ListView.builder(
                        itemCount: state
                            .forecast.forecast.forecastday.first.hour.length,
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index) {
                          var itemHour = state
                              .forecast.forecast.forecastday.first.hour[index];

                          return HourForecastItemCard(
                            item: itemHour,
                            margin: EdgeInsets.only(
                                right: 16.0, left: index == 0 ? 28.0 : 0.0),
                          );
                        },
                      ),
                    ),
                  );
                } else {
                  return SizedBox(
                    height: 150.0,
                    child: ScrollConfiguration(
                      behavior: CleanScrollBehavior(),
                      child: ListView.builder(
                        itemCount: 5,
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index) {
                          return Shimmer.fromColors(
                            child: Container(
                              width: 100.0,
                              margin: EdgeInsets.only(
                                  left: index == 0 ? 28.0 : 16.0),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(20.0),
                              ),
                            ),
                            baseColor: Colors.white.withOpacity(0.04),
                            highlightColor: Colors.white.withOpacity(0.05),
                          );
                        },
                      ),
                    ),
                  );
                }
              },
            ),
          ],
        ),
      ),
      panel: Column(
        children: [
          // tabbar goes here
          // allow user to see the tabbar and navigate the detail info
          const SizedBox(height: 52.0),
          SizedBox(
            height: 40.0,
            child: TabBar(
              controller: _tabController,
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              tabs: const [
                Tab(
                  text: "Info",
                ),
                Tab(
                  text: "Astronomy",
                ),
                Tab(
                  text: "Sports",
                ),
                Tab(
                  text: "Alerts",
                ),
              ],
            ),
          ),

          // the tabbarview, the main info, and detail data
          // goes here
          const SizedBox(height: 28.0),
          SizedBox(
            height: MediaQuery.of(context).size.height - 240.0,
            child: ScrollConfiguration(
              behavior: CleanScrollBehavior(),
              child: TabBarView(
                controller: _tabController,
                children: const [
                  HomeDetailInfoWidget(),
                  HomeDetailAstronomyWidget(),
                  HomeDetailSportWidget(),
                  HomeDetailAlertWidget(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
