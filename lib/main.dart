import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:meteo/app.dart';

void main() async {
  // Before we go we need to
  // ensure the flutter was initialized
  WidgetsFlutterBinding.ensureInitialized();
  await Hive.initFlutter();

  // running meteo app
  // we will run Meteo App

  runApp(const MeteoApp());
}
