part of 'connection_cubit.dart';

enum ConnectionType { mobile, wifi, ethernet }

@immutable
abstract class ConnectivityState {}

class InitialConnectivityState extends ConnectivityState {}

class ConnectedConnectivityState extends ConnectivityState {
  final ConnectionType connectionType;

  ConnectedConnectivityState({
    required this.connectionType,
  });
}

class DisconnectedConnectivityState extends ConnectivityState {}
