part of 'weather_bloc.dart';

@immutable
abstract class WeatherState {}

class InitialWeatherState extends WeatherState {}

class LoadingWeatherState extends WeatherState {}

class SuccessWeatherState extends WeatherState {
  final GeneralForecastPayload forecast;
  final WeatherGeneralSportsPayload sport;

  SuccessWeatherState({
    required this.forecast,
    required this.sport,
  });
}

class NotFoundWeatherState extends WeatherState {}
