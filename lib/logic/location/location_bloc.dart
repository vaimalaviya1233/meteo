import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meteo/data/payloads/weather_payload.dart';
import 'package:meteo/data/repositories/weather_repository.dart';

part 'location_event.dart';
part 'location_state.dart';

class LocationBloc extends Bloc<LocationEvent, LocationState> {
  final WeatherRepository _weatherRepository = WeatherRepository();

  LocationBloc() : super(LocationInitialState()) {
    // ! Searching location event
    on<SearchLocationEvent>((event, emit) async {
      // emit the the loading process
      // will show the loading process while the request running

      emit(SearchingLocationState());

      try {
        // start looking the matched location
        // from weather app support
        var locations = await _weatherRepository.loadLocationsAutoComplete(
            queryLocation: event.locationQuery);

        // we will check the size of locations,
        // check it's empty or not
        if (locations.isNotEmpty) {
          // ok, there some data found
          emit(SearchedLocationState(locations: locations));
        } else {
          // opps sorry, no location found,
          // show the empty state
          emit(EmptySearchLocationState());
        }
      } catch (e) {
        // if some error found,
        // we will show the empty state
        emit(EmptySearchLocationState());
      }
    });
  }
}
