part of 'location_bloc.dart';

@immutable
abstract class LocationEvent {}

class SearchLocationEvent extends LocationEvent {
  final String locationQuery;

  SearchLocationEvent({
    required this.locationQuery,
  });
}
